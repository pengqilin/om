# OM

OM（OPEN-MALL）开源项目六个月左右开发周期，每天都会持续更新代码，敬请期待......



## 前言

OM（OPEN-MALL）项目致力于打造分布式开源电商平台，开源板块包含社区团购（客户端、团长端、供应商端、仓库端、骑手端）、跨境电商、全球购一件代发（商家端）、MT外卖（商家端、骑手配送端）, 本开源项目仅供学习，有不足的地方请留下宝贵的意见。



## 项目文档

- 文档地址：XXX
- 备用地址：XXX



## 项目介绍

OM（OPEN-MALL）项目是分布式开源电商平台，包括前台商城平台及后台管理系统，前台商城系统包含首页门户、商品推荐、商品搜索、商品展示、购物车、订单流程、会员中心、客户服务、帮助中心等模块。后台管理系统包含商品管理、订单管理、会员管理、促销管理、运营管理、内容管理、统计报表、财务管理、权限管理、设置等模块。



### 项目演示

#### 商城管理平台

前端项目`mall-admin-web`地址：XXX

项目演示地址：XXX

#### 社区团购

前端项目`mall-app-web`地址：敬请期待......

项目演示地址：XXX



#### 团长端

前端项目`mall-app-web`地址：敬请期待......

项目演示地址：XXX



#### 商家端

前端项目`mall-app-bussisess`地址：敬请期待......

项目演示地址：XXX



#### 骑手端

前端项目`mall-app-web`地址：敬请期待......

项目演示地址：XXX



#### 跨境电商

前端项目`mall-app-web`地址：敬请期待......

项目演示地址：XXX



#### 全球购一件代发

前端项目`mall-app-web`地址：敬请期待......

项目演示地址：XXX



#### MT外卖

前端项目`mall-app-web`地址：敬请期待......

项目演示地址：XXX



### 项目结构

```XML
<!--模块化-->
<modules>
    <module>mall-common</module> -->  公共代码块 
    <module>mall-getway</module> -->  网关微服务
    <module>mall-pay</module> -->  支付微服务
    <module>mall-report</module> -->  报表统计微服务
    <module>mall-oss</module> -->  文件存储对象微服务
    <module>mall-order</module> -->  订单微服务
    <module>mall-generator</module> -->  自动生成代码
    <module>mall-cart</module> -->  购物车微服务
    <module>mall-member</module> -->  会员微服务
    <module>mall-job</module> -->  分布式任务微服务
    <module>mall-business</module> -->  商家微服务
    <module>mall-goods</module> -->  商品微服务
    <module>mall-auth</module> -->  登录、注册验证授权微服务
    <module>mall-erp</module> -->  仓库管理微服务
    <module>mall-platform</module> -->  平台管理微服务
    <module>mall-search</module> -->  搜索微服务
    <module>mall-market</module> -->  营销管理微服务
    <module>mall-log</module> -->  日志微服务
    <module>mall-sms</module> -->  短信、邮件微服务
    <module>mall-control</module> -->  分布式系统监控微服务
</modules>
```



### 技术选型

#### 后端技术

| 技术 | 说明 | 官网 |
| ---- | ---- | ---- |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |
|      |      |      |



#### 前端技术

| 技术 | 说明 | 官网 |
| ---- | ---- | ---- |
|      |      |      |
|      |      |      |
|      |      |      |



#### 架构图

##### 系统架构图

![](E:\零壹实战\商城-微服务架构图.png)

## 环境搭建

### 开发工具

| 工具 | 说明 | 官网 |
| ---- | ---- | ---- |
|      |      |      |
|      |      |      |
|      |      |      |



### 开发环境

| 工具 | 版本号 | 下载 |
| ---- | ------ | ---- |
|      |        |      |
|      |        |      |
|      |        |      |



### 搭建步骤

```html
Windows环境部署
```

* 



```html
Linux环境部署
```

* 



## Contact-Me

QQ群号：768162721(1群) (进群前，请在网页右上角点star)

微信：18520707666(请加此微信备注意图，再邀请进群技术交流)



##  许可证

[Apache License 2.0](https://github.com/macrozheng/mall-admin-web/blob/master/LICENSE)

Copyright (c) 2018-2020 zero2oneit